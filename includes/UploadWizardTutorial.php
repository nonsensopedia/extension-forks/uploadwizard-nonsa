<?php

/**
 * Class to encapsulate all the html generation associated with the UploadWizard tutorial.
 * Might be a start for a subclass of UploadWizard, if we ever free it of its WMF-oriented features
 * So that non-WMF'ers can use it
 */
class UploadWizardTutorial {

	// Id of imagemap used in tutorial.
	const IMAGEMAP_ID = 'tutorialMap';

	/**
	 * Fetches appropriate HTML for the tutorial portion of the wizard.
	 * Looks up an image on the current wiki. This will work as is on Commons, and will also work
	 * on test wikis that enable instantCommons.
	 * @param String|null $campaign Upload Wizard campaign for which the tutorial should be displayed.
	 * @return String html that will display the tutorial.
	 */
	public static function getHtml( $campaign = null ) {

		$tutorialHtml = wfMessage( 'mwe-upwiz-tutorial' )->parse();

		return $tutorialHtml;
	}
}
