( function ( uw ) {

	var NS_CATEGORY = mw.config.get( 'wgNamespaceIds' ).category;
	var CAT_PREFIX = 'Grafiki – ';
	var FREE_CAT_PREFIX = 'Wolne grafiki – ';
	var LC_PREFIXES = [];
	var FREE_LC_PREFIXES = [];

	for (var i=CAT_PREFIX.length; i>0; i--){
		LC_PREFIXES.push(CAT_PREFIX.toLowerCase().substr(0, i));
	}
	for (var i=FREE_CAT_PREFIX.length; i>0; i--){
		FREE_LC_PREFIXES.push(FREE_CAT_PREFIX.toLowerCase().substr(0, i));
	}

	/**
	 * A categories field in UploadWizard's "Details" step form.
	 *
	 * @extends uw.DetailsWidget
	 */
	uw.CategoriesDetailsWidget = function UWCategoriesDetailsWidget() {
		var categories, catDetails = this;

		uw.CategoriesDetailsWidget.parent.call( this );

		this.categoriesWidget = new mw.widgets.CategoryMultiselectWidget({
			input: {
				value: 'Grafiki – ',
				inputFilter: s => {
					if (s.startsWith(CAT_PREFIX) || s.startsWith(FREE_CAT_PREFIX)) return s;

					var ls = s.toLowerCase();
					//if (ls.startsWith('w')) return FREE_CAT_PREFIX;

					for (var i=0; i<LC_PREFIXES.length; i++) {
						if (ls.startsWith(LC_PREFIXES[i])) {
							return CAT_PREFIX + s.substr(CAT_PREFIX.length - i,
								s.length - CAT_PREFIX.length + i);
						}
					}
					for (var i=0; i<FREE_LC_PREFIXES.length; i++) {
						if (ls.startsWith(FREE_LC_PREFIXES[i])) {
							return FREE_CAT_PREFIX + s.substr(FREE_CAT_PREFIX.length - i,
								s.length - FREE_CAT_PREFIX.length + i);
						}
					}

					if (s.trim().length > 0) return CAT_PREFIX + s.trim();
					return s;
				}
			},
			allowArbitrary: false
		});

		this.categoriesWidget.createTagItemWidget = function ( data ) {
			var widget = this.constructor.prototype.createTagItemWidget.call( this, data );
			if ( !widget ) {
				return null;
			}
			widget.setMissing = function ( missing ) {
				this.constructor.prototype.setMissing.call( this, missing );
				// Aggregate 'change' event
				catDetails.emit( 'change' );
			};
			return widget;
		};

		categories = ( mw.UploadWizard.config.defaults.categories || [] ).filter( function ( cat ) {
			// Keep only valid titles
			return !!mw.Title.makeTitle( NS_CATEGORY, cat );
		} );
		this.categoriesWidget.setValue( categories );

		this.$element.addClass( 'mwe-upwiz-categoriesDetailsWidget' );
		this.$element.append( this.categoriesWidget.$element );

		// Aggregate 'change' event
		this.categoriesWidget.connect( this, { change: [ 'emit', 'change' ] } );
		this.categoriesWidget.allowArbitrary = false;	//UGH
	};
	OO.inheritClass( uw.CategoriesDetailsWidget, uw.DetailsWidget );

	/**
	 * @inheritdoc
	 */
	uw.CategoriesDetailsWidget.prototype.getErrors = function () {
		return $.Deferred().resolve( [] ).promise();
	};

	/**
	 * @inheritdoc
	 */
	uw.CategoriesDetailsWidget.prototype.getWarnings = function () {
		var warnings = [];
		if ( mw.UploadWizard.config.enableCategoryCheck && this.categoriesWidget.getItems().length === 0 ) {
			warnings.push( mw.message( 'mwe-upwiz-warning-categories-missing' ) );
		}
		if ( this.categoriesWidget.getItems().some( function ( item ) {
			return item.missing;
		} ) ) {
			warnings.push( mw.message( 'mwe-upwiz-categories-missing' ) );
		}
		return $.Deferred().resolve( warnings ).promise();
	};

	/**
	 * @inheritdoc
	 */
	uw.CategoriesDetailsWidget.prototype.getWikiText = function () {
		var hiddenCats, missingCatsWikiText, categories, wikiText;

		hiddenCats = [];
		if ( mw.UploadWizard.config.autoAdd.categories ) {
			hiddenCats = hiddenCats.concat( mw.UploadWizard.config.autoAdd.categories );
		}
		if ( mw.UploadWizard.config.trackingCategory ) {
			if ( mw.UploadWizard.config.trackingCategory.all ) {
				hiddenCats.push( mw.UploadWizard.config.trackingCategory.all );
			}
			if ( mw.UploadWizard.config.trackingCategory.campaign ) {
				//hiddenCats.push( mw.UploadWizard.config.trackingCategory.campaign );      //Nonsensopedia uses campaign templates instead
			}
		}
		hiddenCats = hiddenCats.filter( function ( cat ) {
			// Keep only valid titles
			return !!mw.Title.makeTitle( NS_CATEGORY, cat );
		} );

		missingCatsWikiText = null;
		if (
			typeof mw.UploadWizard.config.missingCategoriesWikiText === 'string' &&
			mw.UploadWizard.config.missingCategoriesWikiText.length > 0
		) {
			missingCatsWikiText = mw.UploadWizard.config.missingCategoriesWikiText;
		}

		categories = this.categoriesWidget.getItems().map( function ( item ) {
			return item.data;
		} );

		// add all categories
		wikiText = categories.concat( hiddenCats )
			.map( function ( cat ) {
				return '[[' + mw.Title.makeTitle( NS_CATEGORY, cat ).getPrefixedText() + ']]';
			} )
			.join( '\n' );

		// if so configured, and there are no user-visible categories, add warning
		if ( missingCatsWikiText !== null && categories.length === 0 ) {
			wikiText += '\n\n' + missingCatsWikiText;
		}

		return wikiText;
	};

	/**
	 * @inheritdoc
	 * @return {Object} See #setSerialized
	 */
	uw.CategoriesDetailsWidget.prototype.getSerialized = function () {
		return {
			value: this.categoriesWidget.getItems().map( function ( item ) {
				return item.data;
			} )
		};
	};

	/**
	 * @inheritdoc
	 * @param {Object} serialized
	 * @param {string[]} serialized.value List of categories
	 */
	uw.CategoriesDetailsWidget.prototype.setSerialized = function ( serialized ) {
		this.categoriesWidget.setValue( serialized.value );
	};

}( mw.uploadWizard ) );
